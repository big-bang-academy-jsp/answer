## Section 1

1. What is the purpose of the "key" prop in React?

```
When rendering a list of elements in React, each item inside the list should have a “key” prop with a unique identifier as the value. React can then efficiently update the DOM when the list has been updated by comparing the new key with the previous key. Without the “key” prop, React will have to compare the entire contents of each item in the list, causing slower performance and might lead to elements not updated correctly.
```

2. What is the purpose of the "useEffect" hook in React?

```
The "useEffect" hook is used in React with functional components to manage side effects. It takes a function to be performed as the side effect and an optional array of dependencies. It runs the function after each render unless an array is specified and none of the dependencies have changed since the previous render.
```

3. What is the purpose of the "setState" function in React class components?

```
It is used to update the component’s state by taking an object or function as an argument and merges it with the current state of the component. If a function is passed, the function with receive the previous state and props as argument.

Directly assigning new value to the state will not trigger re-render of the component. Therefore “setState” has to be used to notify React about the change in order to update the component.
```

4. What technique is commonly used to handle authentication and authorization in Node.js?

```
JWT is a common and secure way to handle authentication and authorization in Node.js since it is stateless and allow scaling more easily.
```

5. What is the role of a package manager in Node.js?

```
Package manager in Node.js, such as npm and yarn, helps to manage and install third-party libraries. It handles version control of the dependencies. It also provides commands to install, update and remove the dependencies.
```

## Section 2

```bash
See "server" and "app"
```
